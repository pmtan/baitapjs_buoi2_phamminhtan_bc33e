// Bai 1: Tinh Luong Nhan Vien
function tinhTienLuong() {
  var luongTheoNgay = document.getElementById("luongTheoNgay").value * 1;
  var soNgayLam = document.getElementById("soNgayLam").value * 1;
  var tienLuong = luongTheoNgay * soNgayLam;
  document.getElementById("tienLuong").innerText = tienLuong;
  console.log({ luongTheoNgay, soNgayLam, tienLuong });
}

// Bai 2:
function tinhTrungBinh() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var num4 = document.getElementById("num4").value * 1;
  var num5 = document.getElementById("num5").value * 1;
  var trungBinh = (num1 + num2 + num3 + num4 + num5) / 5;
  document.getElementById("trungBinh").innerHTML = `${trungBinh.toFixed(2)}`;
  console.log({ num1, num2, num3, num4, num5, trungBinh });
}
// Bai 3:
const tiGia = 23500;
document.getElementById("tiGia").innerHTML = tiGia;
function quyDoiTien() {
  var tienUsd = document.getElementById("usd").value * 1;
  if (tienUsd > 0) {
    var tienVnd = tienUsd * tiGia;
    document.getElementById("vnd").innerHTML = `<p> ${tienVnd} VND</p>`;
  } else {
    document.getElementById(
      "vnd"
    ).innerHTML = `<p> Số Tiền Quy Đổi Phải Lớn Hơn 0</p>`;
  }
}

// Bai 4:
function tinhChuViDienTich() {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;
  if (chieuDai > 0 && chieuRong > 0) {
    var chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = chieuDai * chieuRong;
    document.getElementById(
      "chuViDienTich"
    ).innerHTML = `<p> Chu Vi: ${chuVi} , Dien Tich: ${dienTich} </p>`;
  } else {
    document.getElementById(
      "chuViDienTich"
    ).innerHTML = `<p>Chiều dài, chiều rộng phải lớn hơn 0</p>`;
  }
}

// Bai 5
function tinhTongKiSo() {
  var number = document.getElementById("hai-chu-so").value * 1;
  if (number >= 10 && number <= 99) {
    var hangDonVi = number % 10;
    var hangChuc = Math.floor(number / 10);
    var tongKySo = hangChuc + hangDonVi;
    document.getElementById("tongKiSo").innerHTML = tongKySo;
    console.log("Bai 5: Tinh Tong 2 Ky So");
    console.log("So Da Nhap: ", number);
    console.log("Tong Ky So: ", tongKySo);
  } else {
    document.getElementById(
      "tongKiSo"
    ).innerHTML = `<p> Vui lòng nhập số có 2 chữ số </p>`;
  }
}
